function btndraw() {
  var btnbrd = document.getElementById("buttonboard");
  while(btnbrd.lastElementChild) {
    btnbrd.removeChild(btnbrd.lastElementChild);
  }
  if(bot.state.hasStarted) {
    btnbrd.insertAdjacentHTML("beforeend","<button type=\"button\" id=\"timer\" class=\"btn btn-secondary\">Time: " + (Date.now() - bot.state.startTime)/1000 + "</button>");
  }
  else {
    btnbrd.insertAdjacentHTML("beforeend","<button type=\"button\" id=\"timer\" class=\"btn btn-secondary\">Time: 0</button>");
  }
  for(var i = 0;i < btnarr.length;i++) {
    var currobj = btnarr[i];
    var insobj = btnbrd;
    if(typeof currobj.group !== 'undefined') {
      if(document.getElementById(currobj.group + "-group") == null) {
        btnbrd.insertAdjacentHTML("beforeend","<div id=\"" + currobj.group + "-group\" role=\"group\" class=\"btn-group\"></div>");
      }
      insobj = document.getElementById(currobj.group + "-group");
    }
    insobj.insertAdjacentHTML("beforeend","<button type=\"button\" id=\"" + currobj.id + "\" class=\"btn " + currobj.class + "\">" + currobj.name + "</button>");
  }
}
