btn_ctx_funct = () => {
  if(log.length > 0) {
    console.log(log[log.length-1]);
  }
  if(bot.state.hasStarted) {
    btnarr = [];
    if(bot.state.hasCargo) {btnarr.push({name:"Drop Cargo",id:"dropcargo",class:"btn-danger"});}
    else {btnarr.push({name:"Pick Up Cargo",id:"pickcargo",class:"btn-success"});}
    if(bot.state.hasHatch) {btnarr.push({name:"Drop Hatch",id:"drophatch",class:"btn-danger"});}
    else {btnarr.push({name:"Pick Up Hatch",id:"pickhatch",class:"btn-success"});}
    var objcheck = (bot.state.hasCargo || bot.state.hasHatch);
    var rocketcheck = (log[log.length-1].search("rocketzone") != -1);
    var cargocheck = (log[log.length-1].search("cargozone") != -1);
    if(objcheck && rocketcheck) {
        btnarr.push({name:"Rocket Lvl 1",id:"rocket1",class:"btn-primary",group:"rocket"});
        btnarr.push({name:"Lvl 2",id:"rocket2",class:"btn-primary",group:"rocket"});
        btnarr.push({name:"Lvl 3",id:"rocket3",class:"btn-primary",group:"rocket"});
        btnarr.push({name:"Rocket Lvl 1 Fail",id:"rocket1f",class:"btn-danger",group:"rocketf"});
        btnarr.push({name:"Lvl 2 Fail",id:"rocket2f",class:"btn-danger",group:"rocketf"});
        btnarr.push({name:"Lvl 3 Fail",id:"rocket3f",class:"btn-danger",group:"rocketf"});
    }
    if(objcheck && cargocheck) {
        btnarr.push({name:"Cargo",id:"cargo1",class:"btn-info"});
        btnarr.push({name:"Cargo Fail",id:"cargo1f",class:"btn-danger"});
    }
    btndraw();
    if(bot.state.hasCargo) {document.getElementById("dropcargo").ontouchend = () => {bot.state.hasCargo = false;bot.state.hasHatch = false;btn_ctx_funct();};}
    else {document.getElementById("pickcargo").ontouchend = () => {bot.state.hasCargo = true;bot.state.hasHatch = false;btn_ctx_funct();};}
    if(bot.state.hasHatch) {document.getElementById("drophatch").ontouchend = () => {bot.state.hasHatch = false;bot.state.hasCargo = false;btn_ctx_funct();};}
    else {document.getElementById("pickhatch").ontouchend = () => {bot.state.hasHatch = true;bot.state.hasCargo = false;btn_ctx_funct();};}
    var reset = () => {bot.state.hasHatch = false;bot.state.hasCargo = false;btn_ctx_funct();};
    if(objcheck && rocketcheck) {
      document.getElementById("rocket1").ontouchend = reset;
      document.getElementById("rocket2").ontouchend = reset;
      document.getElementById("rocket3").ontouchend = reset;
      document.getElementById("rocket1f").ontouchend = reset;
      document.getElementById("rocket2f").ontouchend = reset;
      document.getElementById("rocket3f").ontouchend = reset;
    }
    if(objcheck && cargocheck) {
      document.getElementById("cargo1").ontouchend = reset;
      document.getElementById("cargo1f").ontouchend = reset;
    }
  }
  else {
    btnarr = [];
    btnarr.push({name:"Start Timer",id:"startbtn",class:"btn-success"});
    if(bot.state.hasCargo) {btnarr.push({name:"Drop Cargo",id:"dropcargo",class:"btn-danger"});}
    else {btnarr.push({name:"Pick Up Cargo",id:"pickcargo",class:"btn-success"});}
    if(bot.state.hasHatch) {btnarr.push({name:"Drop Hatch",id:"drophatch",class:"btn-danger"});}
    else {btnarr.push({name:"Pick Up Hatch",id:"pickhatch",class:"btn-success"});}
    btndraw();
    document.getElementById("startbtn").ontouchend = () => {
      bot.state.hasStarted = true;
      bot.state.startTime = Date.now();
      openFullscreen();
      btn_ctx_funct();
    };
    if(bot.state.hasCargo) {document.getElementById("dropcargo").ontouchend = () => {bot.state.hasCargo = false;bot.state.hasHatch = false;btn_ctx_funct();};}
    else {document.getElementById("pickcargo").ontouchend = () => {bot.state.hasCargo = true;bot.state.hasHatch = false;btn_ctx_funct();};}
    if(bot.state.hasHatch) {document.getElementById("drophatch").ontouchend = () => {bot.state.hasHatch = false;bot.state.hasCargo = false;btn_ctx_funct();};}
    else {document.getElementById("pickhatch").ontouchend = () => {bot.state.hasHatch = true;bot.state.hasCargo = false;btn_ctx_funct();};}
  }
};
