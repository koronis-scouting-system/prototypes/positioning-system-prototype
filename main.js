var cvs = document.getElementById("display");
var ctx = cvs.getContext("2d");
var log = [];
var btn_ctx_funct = () => {};
var btnarr = [];

var field = {
  "year": 2019,
  "color_palette": [
    {"name":"Light Grey","hex":["#cdcdcd"]},
    {"name":"Dark Grey","hex":["#363636"]},
    {"name":"Light Team Color","hex":["#f6acac","#acacf6"]},
    {"name":"Dark Team Color","hex":["#d63333","#3333d6"]},
    {"name":"Transparent","hex":["rgba(0,0,0,0)"]}
  ],
  "field": {"X":54,"Y":27},
  "elements": [
    {
      "name":"field",
      "size":{"X":27,"Y":27},
      "position":{"X":0,"Y":0},
      "color":{"fill":0,"outline":0}
    },
    {
      "name":"habzone",
      "size":{"X":8,"Y":27},
      "position":{"X":0,"Y":0},
      "color":{"fill":0,"outline":3}
    },
    {
      "name":"midzone",
      "size":{"X":18,"Y":27},
      "position":{"X":8,"Y":0},
      "color":{"fill":0,"outline":3}
    },
    {
      "name":"cargohold",
      "size":{"X":4,"Y":15},
      "position":{"X":0,"Y":6},
      "color":{"fill":-1,"outline":3}
    },
    {
      "name":"hab1",
      "size":{"X":4,"Y":13},
      "position":{"X":4,"Y":7},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"hab2",
      "size":{"X":4,"Y":11},
      "position":{"X":0,"Y":8},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"hab3",
      "size":{"X":4,"Y":5},
      "position":{"X":0,"Y":11},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"rocketzone1",
      "size":{"X":9,"Y":5},
      "position":{"X":15,"Y":0},
      "color":{"fill":4,"outline":1}
    },
    {
      "name":"rocket1",
      "size":{"X":3,"Y":2},
      "position":{"X":18,"Y":0},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"rocketzone2",
      "size":{"X":9,"Y":5},
      "position":{"X":15,"Y":22},
      "color":{"fill":4,"outline":1}
    },
    {
      "name":"rocket2",
      "size":{"X":3,"Y":2},
      "position":{"X":18,"Y":25},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"cargozone",
      "size":{"X":11,"Y":11},
      "position":{"X":17,"Y":8},
      "color":{"fill":4,"outline":1}
    },
    {
      "name":"cargoship1",
      "size":{"X":5,"Y":2},
      "position":{"X":22,"Y":11},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"cargoship2",
      "size":{"X":2,"Y":5},
      "position":{"X":20,"Y":11},
      "color":{"fill":3,"outline":1}
    },
    {
      "name":"cargoship3",
      "size":{"X":5,"Y":2},
      "position":{"X":22,"Y":14},
      "color":{"fill":3,"outline":1}
    }
  ],
  localelements: []
};

for(var i = 0;i < field.elements.length;i++) {
  var elemwidth = field.elements[i].size.X;
  var elemheight = field.elements[i].size.Y;
  var xpos = field.elements[i].position.X;
  var ypos = field.elements[i].position.Y;
  field.localelements.push({
    name: field.elements[i].name + "_0",
    x: xpos,
    y: ypos,
    w: elemwidth,
    h: elemheight,
    box: {}
  });
}

for(var i = 0;i < field.elements.length;i++) {
  var elemwidth = field.elements[i].size.X;
  var elemheight = field.elements[i].size.Y;
  var xpos = field.field.X-(field.elements[i].position.X+elemwidth);
  var ypos = field.field.Y-(field.elements[i].position.Y+elemheight);
  field.localelements.push({
    name: field.elements[i].name + "_1",
    x: xpos,
    y: ypos,
    w: elemwidth,
    h: elemheight,
    box: {}
  });
}
