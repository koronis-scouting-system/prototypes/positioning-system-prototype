var xmulti = 0;
var ymulti = 0;
var xoffset = 0;
var yoffset = 0;

function draw(vx, vy, cw, ch, mag, flip, botx, boty) {
  cvs.width = cw;
  cvs.height = ch;
  var xsize = 0;
  var ysize = 0;
  xsize = ch * (field.field.X/field.field.Y);
  ysize = ch;

  xmulti = (xsize/field.field.X)*mag;
  ymulti = (ysize/field.field.Y)*mag;

  xoffset = -vx;
  yoffset = -vy;
  ctx.lineWidth = Math.min(xmulti,ymulti)*0.35;
  for(var i = 0;i < field.elements.length;i++) {
    if(field.elements[i].color.fill >= 0) {
      if(field.color_palette[field.elements[i].color.fill].hex.length > 1) {
        ctx.fillStyle = field.color_palette[field.elements[i].color.fill].hex[flip];
      }
      else {
        ctx.fillStyle = field.color_palette[field.elements[i].color.fill].hex[0];
      }
    }
    else {
      ctx.fillStyle = "rgba(0,0,0,0)";
    }
    if(field.elements[i].color.outline >= 0) {
      if(field.color_palette[field.elements[i].color.outline].hex.length > 1) {
        ctx.strokeStyle = field.color_palette[field.elements[i].color.outline].hex[flip];
      }
      else {
        ctx.strokeStyle = field.color_palette[field.elements[i].color.outline].hex[0];
      }
    }
    else {
      ctx.strokeStyle = "rgba(0,0,0,0)";
    }

    var elemwidth = field.localelements[i].w*xmulti;
    var elemheight = field.localelements[i].h*ymulti;
    var xpos = (field.localelements[i].x+xoffset)*xmulti;
    var ypos = (field.localelements[i].y+yoffset)*ymulti;
    ctx.fillRect(xpos,ypos,elemwidth,elemheight);
    ctx.strokeRect(xpos,ypos,elemwidth,elemheight);
    field.localelements[i].box.w = elemwidth;
    field.localelements[i].box.h = elemheight;
    field.localelements[i].box.x = xpos;
    field.localelements[i].box.y = ypos;

    if(field.elements[i].color.fill >= 0) {
      if(field.color_palette[field.elements[i].color.fill].hex.length > 1) {
        ctx.fillStyle = field.color_palette[field.elements[i].color.fill].hex[flip*-1+1];
      }
      else {
        ctx.fillStyle = field.color_palette[field.elements[i].color.fill].hex[0];
      }
    }
    else {
      ctx.fillStyle = "rgba(0,0,0,0)";
    }
    if(field.elements[i].color.outline >= 0) {
      if(field.color_palette[field.elements[i].color.outline].hex.length > 1) {
        ctx.strokeStyle = field.color_palette[field.elements[i].color.outline].hex[flip*-1+1];
      }
      else {
        ctx.strokeStyle = field.color_palette[field.elements[i].color.outline].hex[0];
      }
    }
    else {
      ctx.strokeStyle = "rgba(0,0,0,0)";
    }

    var elemwidth = field.localelements[i+field.elements.length].w*xmulti;
    var elemheight = field.localelements[i+field.elements.length].h*ymulti;
    var xpos = (field.localelements[i+field.elements.length].x+xoffset)*xmulti;
    var ypos = (field.localelements[i+field.elements.length].y+yoffset)*ymulti;
    ctx.fillRect(xpos,ypos,elemwidth,elemheight);
    ctx.strokeRect(xpos,ypos,elemwidth,elemheight);
    field.localelements[i+field.elements.length].box.w = elemwidth;
    field.localelements[i+field.elements.length].box.h = elemheight;
    field.localelements[i+field.elements.length].box.x = xpos;
    field.localelements[i+field.elements.length].box.y = ypos
  }

  ctx.lineWidth = Math.min(xmulti,ymulti)*0.2;
  ctx.strokeStyle = "rgba(0,0,0,0.1)";
  for(var x = 0;x < field.field.X;x++) {
    for(var y = 0;y < field.field.Y;y++) {
      var elemwidth = xmulti;
      var elemheight = ymulti;
      var xpos = (x+xoffset)*xmulti;
      var ypos = (y+yoffset)*ymulti;
      ctx.strokeRect(xpos,ypos,elemwidth,elemheight);
    }
  }

  ctx.lineWidth = Math.min(xmulti,ymulti)*0.35;
  ctx.fillStyle = "#d63333";
  ctx.strokeStyle = "rgba(0,0,0,0.5)";
  ctx.setLineDash([Math.min(xmulti,ymulti)*0.75,Math.min(xmulti,ymulti)*0.75]);
  ctx.beginPath();
  ctx.moveTo((botx+xoffset)*xmulti,(boty+yoffset-27)*ymulti);
  ctx.lineTo((botx+xoffset)*xmulti,(boty+yoffset+27)*ymulti);
  ctx.stroke();
  ctx.beginPath();
  ctx.moveTo((botx+xoffset-54)*xmulti,(boty+yoffset)*ymulti);
  ctx.lineTo((botx+xoffset+54)*xmulti,(boty+yoffset)*ymulti);
  ctx.stroke();

  ctx.strokeStyle = "#000000";
  ctx.setLineDash([]);
  ctx.beginPath();
  ctx.arc((botx+xoffset)*xmulti,(boty+yoffset)*ymulti,Math.min(xmulti,ymulti)*1,0,Math.PI*2,false);
  ctx.fill();
  ctx.stroke();
}

draw(0,0,window.innerWidth/2,window.innerHeight,1,0);
