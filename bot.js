var bot = {
  "x": 1,
  "y": 14,
  "t": 0,
  "xspeed": 0,
  "yspeed": 0,
  "speedmulti": 0.075,
  "slowdown": true,
  "state": {
    "hasStarted": false,
    "startTime": 0,
    "hasCargo": false,
    "hasHatch": false
  }
};

cvs.addEventListener("touchmove",(event) => {
  var rect = cvs.getBoundingClientRect();
  bot.x = ((event.touches[0].clientX - rect.left)/xmulti - xoffset);
  bot.y = ((event.touches[0].clientY - rect.top)/ymulti - yoffset);
});

cvs.addEventListener("touchstart",(event) => {
  bot.slowdown = false;
});

cvs.addEventListener("touchend",(event) => {
  bot.slowdown = true;
});

drawupdate = setInterval(() => {
  var outstr = "";
  var rect = cvs.getBoundingClientRect();
  for(var i = 0;i < field.localelements.length;i++) {
    if(field.localelements[i].x <= bot.x && field.localelements[i].x + field.localelements[i].w >= bot.x) {
      if(field.localelements[i].y <= bot.y && field.localelements[i].y + field.localelements[i].h >= bot.y) {
        outstr += field.localelements[i].name + " ";
      }
    }
  }
  if(log.length <= 0 || log[log.length-1] != outstr) {
    log.push(outstr);
    btn_ctx_funct();
  }
  if(bot.state.hasStarted) {
    document.getElementById("timer").innerHTML = "Time: " + (Date.now() - bot.state.startTime)/1000;
  }
  bot.x += bot.xspeed;
  bot.y += bot.yspeed;
  if(bot.slowdown) {
    bot.xspeed = (Math.abs(bot.xspeed) < 0.125) ? 0 : bot.xspeed*0.8;
    bot.yspeed = (Math.abs(bot.yspeed) < 0.125) ? 0 : bot.yspeed*0.8;
  }
  var newxv = bot.x*0.5-1;
  draw(newxv,0,window.innerWidth/2,window.innerHeight,1,0,bot.x,bot.y);
}, 50);
